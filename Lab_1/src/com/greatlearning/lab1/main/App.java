package com.greatlearning.lab1.main;

import java.util.Scanner;

import com.greatlearning.lab1.bean.Employee;
import com.greatlearning.service.CredentialService;

public class App {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the First Name");
		String firstName = sc.next();
		System.out.println("Enter the Last Name");
		String lastName = sc.next();
		
		System.out.println("Enter the code for the department from the following"+
		"\n" +
			"1.Technical"+ "\n" +
			"2.Admin" + "\n" +
			"3.Human Resource" + "\n"+
			"4.Legal"
		);
		
		int code = sc.nextInt();
		sc.close();
		
		Employee obj = null;
		
		switch (code) {
			case 1 : obj = new Employee(firstName, lastName, "tech");
				break;
			case 2 : obj = new Employee(firstName, lastName, "admin");
				break ;
			case 3 : obj = new Employee(firstName, lastName, "hr");
				break;
			case 4 : obj = new Employee(firstName, lastName, "legal");
				break;
			default: System.out.println("Invalid output");	
		}
		System.out.println("Dear "+obj.getFirstName()+" your generated credentials are as follows");
		CredentialService data = new CredentialService() ;
		data.emailId(obj);
		data.password();

	}

}
