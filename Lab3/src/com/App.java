package com;

import java.util.Scanner;
import java.util.Stack;

class StackBalanceOperation{
	public static boolean checkBalanced(String data) {
		Stack <Character> stack = new Stack<>();
		for(int i =0 ; i < data.length() ; i++) {
			char ch = data.charAt(i);
			if(ch == '[' || ch == '{' || ch == '(' ) {
				stack.push(ch);
				continue;
			}
			if (stack.isEmpty()) {
				return false ;
			}
			char c ;
			
			switch(ch) {
			case ')' : 
				c = stack.pop();
				if (c == '{' || c == '[') {
					return false;
				}
				break;
				
			case '}' : 
				c = stack.pop();
				if (c == '(' || c == '[') {
					return false;
				}
				break;
				
			case ']' : 
				c = stack.pop();
				if (c == '{' || c == '(') {
					return false;
				}
				break;
			}
		}
		return stack.isEmpty() ;
	}
}

public class App {

	public static void main(String[] args) {
		Scanner obj = new Scanner(System.in);
		System.out.println("Enter the String");
		String data = obj.next();
		boolean result = StackBalanceOperation.checkBalanced(data);
		if (result) {
			System.out.println("String is balanced");
		}
		else {
			System.out.println("String is not balanced");
		}
	}

}
